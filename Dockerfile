FROM debian:stretch
LABEL authors="Jeff Geerling, Abel Luck"

ENV DEBIAN_FRONTEND noninteractive

ENV pip_packages "ansible docker molecule cryptography"

# Install dependencies.
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
       sudo systemd cron anacron gnupg \
       build-essential libffi-dev libssl-dev \
       python-pip python-dev python-setuptools python-wheel \
       python3 python3-pip python3-dev python3-setuptools python3-wheel \
    && rm -rf /var/lib/apt/lists/* \
    && rm -Rf /usr/share/doc && rm -Rf /usr/share/man \
    && apt-get clean

# Install Ansible via pip.
RUN pip3 install $pip_packages

COPY initctl_faker .
RUN chmod +x initctl_faker && rm -fr /sbin/initctl && ln -s /initctl_faker /sbin/initctl

# Install Ansible inventory file.
RUN mkdir -p /etc/ansible
RUN echo "[local]\nlocalhost ansible_connection=local" > /etc/ansible/hosts

VOLUME ["/sys/fs/cgroup"]
CMD ["/lib/systemd/systemd"]
